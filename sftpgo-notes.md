Set default AWS creds in `/var/lib/sftpgo/.aws/credentials`

```
[default]
aws_access_key_id=
aws_secret_access_key=
```