License Server Kickstart


1. Convert license over to server license, Add the following to `/usr/local/flexlm/licenses/license.dat`

`SERVER matlab-lic.wvu.edu 06CC2645A3B8 27000`

`DAEMON MLM /mnt/licenseManager/etc/glnxa64/MLM`

2. Create SSL key 

`grep passphrase /mnt/licenseManagerDashboard/node_modules/license-manager-for-matlab-dashboard/*.js`

`cd /mnt/licenseManagerDashboard/node_modules/license-manager-for-matlab-dashboard/certificates/default/`

`openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout key -out crt`

`openssl pkcs12 -export -inkey key -in crt -out test.pfx`

3. Check system status

`systemctl stop nlm{,dashboard}.service`

`systemctl start nlm{,dashboard}.service`

`systemctl status nlm{,dashboard}.service`

